package fs

import "os"

// Check if file exist
func FileExists(path string) bool {

	info, err := os.Stat(path)

	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

// Check if dir exists
func DirExists(path string) bool {

	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}

	return true
}
